#!/usr/bin/env python3

import os
from shutil import copyfile

srcPath = './music/'
dstPath = '../public/songs/'

files = []
# r=root, d=directories, f = files
for r, d, f in os.walk(srcPath):
    for file in f:
        path = os.path.join(r, file)
        if '.mp3' not in file:
            print('Found a non mp3 file: ' + path)
            continue

        if '_ID_' not in path:
            print('Found a file without the _ID_ tag:' + path)
            continue

        id = path.split('/')[-1].split('_ID_')[0]
        dPath = dstPath + id + '.mp3'
        print('Copying ' + path + ' to ' + dPath)
        copyfile(path, dPath)