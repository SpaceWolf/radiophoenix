#!/usr/bin/env python3

import os
from shutil import copyfile
import xml.etree.ElementTree as ET
import requests

dstPath = '../public/songs/'
xmlLocation = 'songs2019-06-08.xml'

baseUrl = 'http://radiophoenix.corps.nl/stopspreadingit18/'


root = ET.parse(xmlLocation).getroot()
songs = root.findall(".//SONG")
for song in songs:
    # print(song.find('LINK').text)
    # print(song.find('ID').text)

    ext = song.find('LINK').text.split('/')[-1].split('.')[-1]

    url = baseUrl + song.find('LINK').text
    print('Downloading id: ' + song.find('ID').text + ' url: ' + url)
    r = requests.get(url)
    if r.status_code != 200:
        print('\tGot a non 200 response')
    open(song.find('ID').text + '.' + ext, 'wb').write(r.content)


# for type_tag in root.findall('bar/type'):
#     value = type_tag.get('foobar')
#     print(value)

#     https://docs.python.org/3/library/xml.etree.elementtree.html

