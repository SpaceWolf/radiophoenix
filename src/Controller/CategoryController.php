<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

use App\Entity\Category;
use App\Entity\Song;

class CategoryController extends AbstractController
{
    /**
     * @Route("/admin/category/list", name="category_list")
     */
    public function list()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Category::class);

        return $this->render('category/list.html.twig', [
            'topLevelCategories' => $repo->findTopLevel(),
        ]);
    }

    /**
     * @Route("/getCategoryTree", name="category_tree")
     */
    public function categoryTree()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Category::class);

        $tlc = $repo->findTopLevel();

        $tree = '<?xml version="1.0" encoding="UTF-8"?>
            <DATA>
                <SONGS>';
        foreach ($tlc as $cat) {
            $tree .= $this::recursiveGet($cat);
        }

        $tree .= '
            </SONGS>
        </DATA>';

        $response = new Response($tree);
        $response->headers->set('Content-Type', 'xml');
        return $response;
    }

    private function recursiveGet($category)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Category::class);
        $repo_s = $em->getRepository(Song::class);

        $tree = '
            <FOLDER>
                <NAME>'.htmlspecialchars($category->getName()).'</NAME>
                <ID>'.$category->getId().'</ID>
                <SONGS>';

        foreach ($repo_s->getSongs($category) as $song) {
            $tree .= '
                <SONG>
                    <ID>'.$song->getId().'</ID>
                    <TITLE>'.htmlspecialchars($song->getName()).'</TITLE>
                    <LINK>'.'song/'. $song->getId() .'</LINK>
                    <DOWNLOAD>1</DOWNLOAD>
                    <PLAYED>0</PLAYED>
                </SONG>';
        }

        foreach ($repo->getChildren($category) as $child) {
            $tree .= $this::recursiveGet($child);
        }

        $tree .= '
                </SONGS>
            </FOLDER>';
        return $tree;
    }

    // /**
    //  * @Route("/treeImport", name="tree_import")
    //  */
    // public function treeImport(Request $request)
    // {
    //     $file = $request->files->get('file');
    //     file_get_contents($file->getRealPath());

    //     $encoder = new XmlEncoder();
    //     $out = $encoder->decode(file_get_contents($file->getRealPath()), 'xml');

    //     dump($out);

    //     foreach ($out['SONGS']['FOLDER'] as $folder) {
    //         $this::recursiveSet($folder);
    //     }

    //     return new Response('Succes?!');
    // }

    private function recursiveSet($tree, $parentCategory = null)
    {
        $em = $this->getDoctrine()->getManager();

        // Make the current item into db
        $cat = new Category();
        $cat->setName($tree['NAME']);
        $cat->setParent($parentCategory);
        $em->persist($cat);
        $em->flush();

        // Make all the songs
        // If only one song in folder
        if (isset($tree['SONGS']['SONG']['TITLE'])) {
            $song = $tree['SONGS']['SONG'];
            $si = new Song();
            $si->setName($song['TITLE']);
            $si->setCategory($cat);
            $si->setLegacyId($song['ID']);
            $a = explode('/', $song['LINK']); // array
            $a = end($a); // sdasdasdas.mp3
            $a = explode('.', $a); // array
            $a = end($a); // mp3
            $a = $song['ID'] . '.' . $a;
            $si->setFile($a);
            $em->persist($si);
        } elseif (isset($tree['SONGS']['SONG'])) {
            foreach ($tree['SONGS']['SONG'] as $song) {
                $si = new Song();
                $si->setName($song['TITLE']);
                $si->setCategory($cat);
                $si->setLegacyId($song['ID']);
                $a = explode('/', $song['LINK']); // array
                $a = end($a); // sdasdasdas.mp3
                $a = explode('.', $a); // array
                $a = end($a); // mp3
                $a = $song['ID'] . '.' . $a;
                $si->setFile($a);
                $em->persist($si);
            }
        }
        $em->flush();

        // Call recursiveSet on all children categories
        // If only 1 subfolder
        if (isset($tree['SONGS']['FOLDER']['NAME'])) {
            $this::recursiveSet($tree['SONGS']['FOLDER']);
        } elseif (isset($tree['SONGS']['FOLDER'])) { // If no folder
            foreach ($tree['SONGS']['FOLDER'] as $folder) {
                $this::recursiveSet($folder, $cat);
            }
        }
    }
}
