<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\User;
use App\Entity\Song;
use App\Entity\Category;

use App\Form\SongForm;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_home")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'items' => [
                'new_song',
                'list_songs',
                'app_logout'
            ],
        ]);
    }

    /**
     * @Route("/admin/song/new", name="new_song")
     */
    public function newSongAction(Request $request)
    {
        $song = new Song();

        $form = $this->createForm(SongForm::class, $song);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $musicFile = $form['songFile']->getData();

            if ($musicFile) {
                $originalFilename = pathinfo($musicFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $newFilename = preg_replace( '/[^a-z0-9]+/', '-', strtolower( $originalFilename)) .'-'.uniqid().'.'.$musicFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $musicFile->move(
                        $this->getParameter('song_dir'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    $this->addFlash(
                        'notice',
                        'Failed!'
                    );
                }

                // updates the 'musicFilename' property to store the PDF file name
                // instead of its contents
                $song->setFile($newFilename);
            }

            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $song = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $em->persist($song);
            $em->flush();

            $this->addFlash(
                'notice',
                'Success!'
            );
        }

        return $this->render('admin/new-song.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/song/{id}", name="edit_song", requirements={"id"="\d+"})
     */
    public function editSongAction(Request $request, Song $song)
    {
        $form = $this->createForm(SongForm::class, $song);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $musicFile = $form['songFile']->getData();

            if ($musicFile) {
                $originalFilename = pathinfo($musicFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $newFilename = preg_replace( '/[^a-z0-9]+/', '-', strtolower( $originalFilename)) .'-'.uniqid().'.'.$musicFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $musicFile->move(
                        $this->getParameter('song_dir'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    $this->addFlash(
                        'notice',
                        'Failed!'
                    );
                }

                // updates the 'musicFilename' property to store the PDF file name
                // instead of its contents
                $song->setFile($newFilename);
            }

            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $song = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $em->persist($song);
            $em->flush();

            $this->addFlash(
                'notice',
                'Success!'
            );
        }

        return $this->render('admin/edit-song.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/song/list", name="list_songs")
     */
    public function list(Request $request)
    {
        $songs = $this->getDoctrine()
            ->getRepository(Song::class)
            ->findAll();

        return $this->render('admin/list-songs.html.twig', [
            'songs' => $songs
        ]);
    }

//     /**
//      * @Route("/createUser")
//      */
//     public function createUserAction(UserPasswordEncoderInterface $passwordEncoder)
//     {
//         $user = new User();

//         // $encoder = $factory->getEncoder($user);
//         // $user->setSalt(md5(time()));
//         $user->setUsername('spacey9');
//         $user->setPassword($passwordEncoder->encodePassword($user, 'oF66F1R&y9D4sRB5Ih&I4m9N'));

//         $em = $this->getDoctrine()->getEntityManager();
//         $em->persist($user);
//         $em->flush();

//         return new Response('Sucessful');
//     }
}
