<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

use App\Entity\Song;

class SongController extends AbstractController
{
    // /**
    //  * @Route("/importAmountPlayed", name="song")
    //  */
    // public function importAmountPlayed(Request $request)
    // {
    //     $statstr = '';

    //     $em = $this->getDoctrine()->getManager();
    //     $repo = $em->getRepository(Song::class);

    //     $file = $request->files->get('file');
    //     file_get_contents($file->getRealPath());

    //     $encoder = new XmlEncoder();
    //     $out = $encoder->decode(file_get_contents($file->getRealPath()), 'xml');

    //     dump($out);

    //     foreach ($out['PLAYED'] as $item) {
    //         $song = $repo->findOneByLegacyId($item['ID']);
    //         if (!$song) {
    //             $statstr .= 'Could not find song in DB. legacyId: ' . $item['ID'] . '\n';
    //             continue;
    //         }

    //         dump($item);
    //         $song->setTimesPlayed($item['AMOUNT']);
    //         $em->persist($song);
    //     }
    //     $em->flush();

    //     return new Response('<pre>Succes!\n' . $statstr . '</pre>');
    // }

    /**
     * @Route("getAmountPlayed", name="get_amount_played")
     */
    public function getAmountPlayed()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Song::class);

        $songs = $repo->findAll();
        $out = '<?xml version="1.0" encoding="UTF-8"?>
        <DATA>';
        foreach ($songs as $song) {
            $out .= '
                <PLAYED>
                    <LINK>'.'song/'. $song->getId().'</LINK>
                    <AMOUNT>'.$song->getTimesPlayed().'</AMOUNT>
                    <ID>'.$song->getId().'</ID>
                </PLAYED>';
        }
        $out .= '</DATA>';

        $response = new Response($out);
        $response->headers->set('Content-Type', 'xml');
        return $response;
    }

    /**
     * @Route("/song/{song}", name="get_song", requirements={"song"="\d+"})
     */
    public function getSong(Song $song)
    {
        $em = $this->getDoctrine()->getManager();
        $song->setTimesPlayed($song->getTimesPlayed()+1);
        $em->persist($song);
        $em->flush();

        // i.e Sending a file from the resources folder in /web
        // in this example, the TextFile.txt needs to exist in the server
        $folderPath = $this->getParameter('song_dir');
        $filename = $song->getFile();

        // This should return the file located in /mySymfonyProject/web/public-resources/TextFile.txt
        // to being viewed in the Browser
        try {
            $response = new BinaryFileResponse($folderPath.$filename);
        } catch (\Exception $e){
            dump($e);
            if($e instanceof FileNotFoundException) {
                return new Response("Song not found", 404);
            }
            return new Response("Error", 500);
        }
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $song->getId().'.mp3');
        return $response;
    }
}
