<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    /**
     * @return Category[] Returns the categories without a parent
     */
    public function findTopLevel($showHidden = FALSE)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.parent is null')
            ->orderBy('c.name', 'ASC')
            ;

        if (!$showHidden) {
            $qb = $qb->andWhere('c.hidden = FALSE');
        }

        return $qb->getQuery()
            ->getResult();
    }

    /**
     * @return Category[] Returns the children of a category
     */
    public function getChildren(Category $category, $showHidden = FALSE)
    {
        $qb = $this->createQueryBuilder('c')
            ->andWhere('c.parent = :cat')
            ->setParameter('cat', $category)
            ->addOrderBy('c.name', 'ASC')
            ;

        if (!$showHidden) {
            $qb = $qb->andWhere('c.hidden = FALSE');
        }

        return $qb->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
